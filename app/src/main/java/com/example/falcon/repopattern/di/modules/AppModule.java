package com.example.falcon.repopattern.di.modules;


import android.content.Context;

import com.example.falcon.repopattern.data.AppDB;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    Context context;

    public AppModule(Context context) {
        this.context = context;
    }

    @Provides
    Context getContext(){
        return context;
    }

    @Provides
    AppDB getAppDB(){
        return AppDB.getInstance(context);
    }

}
