package com.example.falcon.repopattern;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.falcon.repopattern.data.AppDB;
import com.example.falcon.repopattern.data.entity.Author;
import com.example.falcon.repopattern.data.repository.AuthorRepository;
import com.example.falcon.repopattern.ui.adapter.AuthorAdapter;
import com.example.falcon.repopattern.viewmodel.MainViewModel;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.operators.flowable.FlowableDoOnEach;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();
    @Inject
    AuthorRepository authorRepository;

    @BindView(R.id.authors_recycler)
    RecyclerView authorsRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((App) getApplication()).getAuthorComponent().inject(this);
        ButterKnife.bind(this);
        MainViewModel vmodel = new MainViewModel(authorRepository);
        final AuthorAdapter authorAdapter = new AuthorAdapter(new ArrayList<Author>(),this);
        authorsRecycler.setAdapter(authorAdapter);
        authorsRecycler.setLayoutManager(new LinearLayoutManager(this));
        vmodel.getAuthorsList().subscribeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Author>() {
            @Override
            public void accept(Author author){
                Log.d(TAG, "accept: " + author.getName());
                authorAdapter.addAuthorToList(author);
            }
        });
//        authorRepository.getAllAuthors().subscribeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<String>>() {
//            @Override
//            public void accept(List<String> strings) throws Exception {
//                for(String name : strings){
//                    Log.d(TAG, "accept: " + name );
//                }
//            }
//        });
//
////        Observer<List<Author>> obs = new Observer<List<Author>>() {
////            @Override
////            public void onSubscribe(Disposable d) {
////                Log.e(TAG, "onSubscribe: ");
////            }
////
////            @Override
////            public void onNext(List<Author> authors) {
////                Log.e(TAG, "onNext: " + authors.size() );
////            }
////
////            @Override
////            public void onError(Throwable e) {
////                Log.e(TAG, "onError: ",e);
////            }
////
////            @Override
////            public void onComplete() {
////                Log.e(TAG, "onComplete: ");
////            }
////        };
////
////        authorRepository.getAllAuthors().observeOn(Schedulers.io()).subscribeOn(AndroidSchedulers.mainThread()).subscribe(obs);
//
//
////        authorRepository.getAllAuthors().observeOn(Schedulers.io()).subscribeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<Author>>() {
////            @Override
////            public void accept(List<Author> authors) {
////                for (Author author : authors) {
////                    Log.d(TAG, "author name: " + author.getName());
////                }
////            }
////        }, new Consumer<Throwable>() {
////            @Override
////            public void accept(Throwable throwable) throws Exception {
////                Log.e(TAG, "accept: ",throwable );
////            }
////        });
    }
}
