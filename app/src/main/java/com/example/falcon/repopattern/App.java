package com.example.falcon.repopattern;

import android.app.Application;

import com.example.falcon.repopattern.di.component.AuthorComponent;
import com.example.falcon.repopattern.di.component.DaggerAuthorComponent;
import com.example.falcon.repopattern.di.modules.AppModule;
import com.example.falcon.repopattern.di.modules.AuthorModule;

public class App extends Application {

    AuthorComponent authorComponent;
    @Override
    public void onCreate() {
        super.onCreate();
        authorComponent = DaggerAuthorComponent.builder().appModule(new AppModule(getApplicationContext())).authorModule(new AuthorModule()).build();
    }

    public AuthorComponent getAuthorComponent(){
        return this.authorComponent;
    }
}
