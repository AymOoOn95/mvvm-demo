package com.example.falcon.repopattern.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.falcon.repopattern.data.dao.AuthorDao;
import com.example.falcon.repopattern.data.entity.Author;

@Database(entities = {Author.class},version = 1)
public abstract class AppDB extends RoomDatabase {

    private static AppDB instance ;

    public abstract AuthorDao authorDao();

    public static AppDB getInstance(Context context){
        if(instance == null){
            instance =  Room.databaseBuilder(context,AppDB.class,"app.db").build();
        }
        return instance;
    }
}
