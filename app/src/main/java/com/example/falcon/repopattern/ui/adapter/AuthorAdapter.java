package com.example.falcon.repopattern.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.falcon.repopattern.R;
import com.example.falcon.repopattern.data.entity.Author;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AuthorAdapter extends RecyclerView.Adapter<AuthorAdapter.AuthorViewHolder> {

    List<Author> authorList;
    Context context;

    public AuthorAdapter(List<Author> authorList, Context context) {
        this.authorList = authorList;
        this.context = context;
    }

    @NonNull
    @Override
    public AuthorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.author_item_layout,parent,false);
        return new AuthorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AuthorViewHolder holder, int position) {
        holder.authorName.setText(authorList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return authorList.size();
    }

    public List<Author> getAuthorList() {
        return authorList;
    }

    public void setAuthorList(List<Author> authorList) {
        this.authorList = authorList;
    }

    public void addAuthorToList(Author author){
        authorList.add(author);
//        notifyItemRangeChanged(authorList.size() - 1,authorList.size());
    }

    public class AuthorViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.author_name)
        TextView authorName;
        public AuthorViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
