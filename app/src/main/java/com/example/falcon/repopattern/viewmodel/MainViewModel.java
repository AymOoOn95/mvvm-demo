package com.example.falcon.repopattern.viewmodel;

import com.example.falcon.repopattern.data.entity.Author;
import com.example.falcon.repopattern.data.repository.AuthorRepository;
import io.reactivex.Flowable;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

public class MainViewModel {

    AuthorRepository repo;

    public MainViewModel(AuthorRepository repo) {
        this.repo = repo;
    }

    public Flowable<Author> getAuthorsList(){
        return repo.getAllAuthors().filter(new Predicate<Author>() {
            @Override
            public boolean test(Author author){
                if(author.getId()%2 == 0){
                    return true;
                }else {
                    return false;
                }
            }
        }).observeOn(Schedulers.io());
    }
}
