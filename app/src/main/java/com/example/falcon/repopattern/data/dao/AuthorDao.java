package com.example.falcon.repopattern.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.falcon.repopattern.data.entity.Author;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface AuthorDao{

    @Insert
    void addAuthor(Author author);

    @Query("select * from Author")
    Flowable<List<Author>> getAllAuthors();
}
