package com.example.falcon.repopattern.di.modules;

import com.example.falcon.repopattern.data.AppDB;
import com.example.falcon.repopattern.data.repository.AuthorRepository;

import dagger.Module;
import dagger.Provides;

@Module
public class AuthorModule {


    // todo review it again
    @Provides
    AuthorRepository authorRepository(AppDB appDB){
        return new AuthorRepository(appDB);
    }
}
