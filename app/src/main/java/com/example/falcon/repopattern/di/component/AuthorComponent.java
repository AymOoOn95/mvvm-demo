package com.example.falcon.repopattern.di.component;

import com.example.falcon.repopattern.di.modules.AppModule;
import com.example.falcon.repopattern.di.modules.AuthorModule;
import com.example.falcon.repopattern.MainActivity;

import dagger.Component;

@Component(modules = {AppModule.class,AuthorModule.class})
public interface AuthorComponent {


    void inject(MainActivity mainActivity);
}
